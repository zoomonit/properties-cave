#
# $Id$
# vim6:ts=2:sw=2:et:syntax=ruby

class dwarfgarden (
  $basedir = '/opt/dwarfgarden',
  $dbname,
  $dbuser,
  $dbpassword ) {

  require pgsql

#  require pgsql pourrait être ajouté: à tester
#  la classe pgsql est ajoutée dans nodes.pp
#
#  if $monitored {
#    include dwarf-garden::zabbix
#  }

  user::dir { $basedir: } -> 
  user::dir {"${basedir}/database": } ->
  user::file { "${basedir}/database/dwarfgarden.schema.sql":          source => "puppet:///modules/${title}/database/dwarfgarden.schema.sql" } 
  user::file { "${basedir}/database/dwarfgarden.function.sql":        source => "puppet:///modules/${title}/database/dwarfgarden.function.sql" } 
  user::file { "${basedir}/database/dwarfgarden.constraints.sql":     source => "puppet:///modules/${title}/database/dwarfgarden.constraints.sql" } 
  user::file { "${basedir}/database/dwarfgarden.schema_update01.sql": source => "puppet:///modules/${title}/database/dwarfgarden.schema_update01.sql" } 
  user::file { "${basedir}/database/dwarfgarden.schema_update02.sql": source => "puppet:///modules/${title}/database/dwarfgarden.schema_update02.sql" } 

  pgsql::rolecreate{"$dbuser":
    password => "$dbpassword",
    option   => 'login',
  } -> 
  pgsql::dbcreate{"$dbname":
    dbuser => $dbuser,
  } ->
  pgsql::psql { "${basedir}/database/dwarfgarden.schema.sql" :          dbname  =>  $dbname, dbuser => $dbuser } ->
  pgsql::psql { "${basedir}/database/dwarfgarden.constraints.sql" :     dbname  =>  $dbname, dbuser => $dbuser } ->
  pgsql::psql { "${basedir}/database/dwarfgarden.schema_update01.sql" : dbname  =>  $dbname, dbuser => $dbuser } ->
  pgsql::psql { "${basedir}/database/dwarfgarden.schema_update02.sql" : dbname  =>  $dbname, dbuser => $dbuser } ->
  pgsql::psql { "${basedir}/database/dwarfgarden.function.sql" :        dbname  =>  $dbname, dbuser => $dbuser, replay => true }

  user::userbinfile { 'genDwarf.sh':                            source => "puppet:///modules/${title}/genDwarf.sh" }
  user::userbinfile { 'setDwarf.sh':                            source => "puppet:///modules/${title}/setDwarf.sh" }

  logrotate::conffile { 'ipc' :
    source  => 'puppet:///modules/dwarfgarden/logrotate.d.ipc',
  }

}
