#Ce script permet de générer un nain......
#!/bin/bash

_help() {
	#echo "$0 --from pf source --to pf cible"
	#echo "$0 --version --on pf cible"
	echo "$0 --consult pf cible"
	echo "$0 --crDwarf pf cible "
}

#############
#           #
 #Fonctions#
#           #
#############


parseParams() {
	

	while [ "$1" != "" ]
	do
		case $1 in
			--from)
			pfs=$2
			shift;;
			--to)
			pfc=$2
			
			shift;;
			--version)
			version=$2
			shift;;
			--on)
			pfcible=$2
			shift;;
			--consult)
			pfconsult=$2
			shift;;
			--crDwarf)
			pfd=$2
			shift;;
			--help|-h)
			_help
			exit 1
			;;
			*)
			_help
			;;
		esac
		shift
	done
} 

#getTemplate() {
#	echo plop
#}

#launchInstallIris() {
#	echo plop2
#}

Consult_Version(){
 psql -U postgres -d dwarfgarden -At -c " select machine.hostname, application.name from application, machine where application.id = properties.fk_application and machine.hostname = '$version';  "


}
Create_dwarf(){
echo "create dwarf for $pfd"

psql -U postgres -d dwarfgarden <<'EOF'
create or replace function gendwarf(plateforme text) returns setof properties
as $$
(select p.*
from properties p 
join relation_env_properties r on r.fk_properties = p.id 
where r.fk_environnement = (select fk_environnement from plateforme where nom = $1) 
and p.fk_plateforme is null) 
UNION 
(select p.*
from properties p 
where p.fk_plateforme = $1) ;
$$ language sql ;
EOF

psql -U postgres -d dwarfgarden -At -F'=' -c "select nom,valeur from gendwarf('$pfd') order by fk_plateforme,nom" > $pfd.iris

}

Display_properties(){
echo " display propertie from $pfconsult"
psql -U postgres -d dwarfgarden <<'EOF'
create or replace function gendwarf(plateforme text) returns setof properties
as $$
(select p.*
from properties p 
join relation_env_properties r on r.fk_properties = p.id 
where r.fk_environnement = (select fk_environnement from plateforme where nom = $1) 
and p.fk_plateforme is null) 
UNION 
(select p.*
from properties p 
where p.fk_plateforme = $1) ;
$$ language sql ;
EOF

psql -U postgres -d dwarfgarden -At -F'=' -c "select nom,valeur from gendwarf('$pfconsult') order by fk_plateforme,nom"
}

#############
 #  Begin  #
#############

parseParams $*

if [ "$pfconsult" != "" ]
then
Display_properties $pfconsult
elif [ "$version" != "" ]
then
Consult_Version $version
elif [ "$pfd" != "" ]
then
Create_dwarf $pfd
fi
