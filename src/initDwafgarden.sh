#!/bin/bash
. $(dirname "$0")/lib/common.sh

#pf="qua-db1"
pf="strator-azo"
dbname="dwarfgarden"
if 
	[ $pf !=  $(hostname -s) ]
then
	echo ""$(hostname -s)" is not authorised plateform, only $pf is supported" && exit 1
fi

#psqlcommand="psql -At -U postgres"
#liste des machines
file="$(cat $(whereis_commonslib platforms))"
list="$(cat $(whereis_commonslib platforms) | sed -r 's/.*hostname=([^;]+).*/\1/' | sort -u)"

#cat list.txt  | sed -r -e "s/hostname=([^=;]+);ip=(.+)$/hostname='\1';ip='\2'/g"
#awk -F ";" '{print $4 "|" $5}' lib/platforms
#UPDATE machine set hostname = $machine
#for machine in $(cat $(whereis_commonslib platforms) | sed -r 's/.*hostname=([^;]+).*/\1/' | sort -u) ; do ""psql -U postgres -d dwarfgarden -c "insert into machine (hostname) values ('$machine');"""; done
#for machine in $(cat $(whereis_commonslib platforms) | sed -r 's/.*hostname=([^;]+).*/\1/' | sort -u) ; do echo "psql -At -U postgres UPDATE machine set hostname ='$machine';"; done
#for ip in $(cat $(whereis_commonslib platforms) | sed -r 's/.*ip=([^;]+).*/\1/' | sort -u) ; do echo "UPDATE machine set ip ='$ip';"; done

#initialisation des tables:
#"$(cat $(whereis_commonslib platforms) | sed -r -e 's/^type=([^;]+).*/\1/' | sort -u)"

#machine
val="$(cat $(whereis_commonslib platforms) | sed -r -e "s/.*hostname=([^=;]+);ip=(.+);$/('\1','\2'),/g" -e '$s/,$//' |sort -u |tac)"

_updateMachine()
{
	local query=$( echo "
begin;
insert into server (hostname, ip)
				values
				"$val";
	--Mise a jour du champ description
		UPDATE server SET description = 'db' where hostname ilike '%-db%';
		UPDATE server SET description = 'app' where hostname ilike '%-app%';
		UPDATE server SET description = 'web' where hostname ilike '%-web%';
END;")
#echo $query
psql -At -q -U postgres -d $dbname <<< "$query"
}

#table environnement:

_updateEnvironnement()
{
	local query=$( echo "
begin;
	INSERT INTO environment (name, description, role)
				values 
				('GDP', 'gdp', NULL), 
				('QUA', 'qualification', 'IRIS'), 
				('REC', 'user acceptance france', 'IRIS'), 
				('PROD','production', 'IRIS'), 
				('DEV', 'development', 'IRIS'), 
				('DEMO', 'demonstration', 'IRIS'), 
				('UAT', 'uat', 'IRIS'),
				('ANY','meta environment that represents all environment. Used in properties table',NULL); 
end;")
	psql -At -q -U postgres -d $dbname <<< "$query"
			}
#table plateforme

_updatePlateforme()

{
	local nom="$(cat $(whereis_commonslib platforms) |cut -d '=' -f3 |sort -u |sed -e 's/;.*$//')"
#fk_environnement qualif rec prod dev demo
	for n in $nom; 
		do psql -At -U postgres -d dwarfgarden <<-EOF
			insert into platform (name, environment) values (upper('$n'), 
				case when '$n' ilike '%qua%' then 'QUA'
				when '$n' ilike 'prod%' then 'PROD'
				when '$n' ilike 'rec%' then 'REC'
				when '$n' ilike 'demo%' then 'DEMO'
				when '$n' ilike '%gdp%' then 'GDP'
				when '$n' ilike 'dev%' then 'DEV'
				when '$n' ilike 'uat%' then 'UAT' 
				else null
				end ); 
		EOF
	done
	
	local query=$( echo "INSERT INTO platform (name,environment) values ('ANY','ANY');")
	psql -At -q -U postgres -d $dbname <<< "$query"
}



_updateMachine
_updateEnvironnement
_updatePlateforme
