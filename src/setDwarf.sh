#!/bin/bash
# $Id$

DB=dwarfgarden
DB_USER=dwarfgarden

_help() {
	cat <<-EOF
	$0 [--help] --file|-f Dwarf.iris --env qua|dev|prod [--pf qua1|dev1|prod2] [(--version|-V) version]

	EOF
}

trim() {
	echo "$1" |  sed -e 's/^[[:space:]]*//g' -e 's/[[:space:]]*$//g'
}

parse_opts() {
	while [ -n "$1" ]
	do
		case $1 in
			-f|--file)
			   propfile=$2
			   shift
			   ;;
			--env)
				env=$2
				shift
				;;
			--pf)
				pf=$2
				shift
				;;
			-V|--version)
				version=$2
				shift
				;;
			--help)
				_help
				exit
				;;
			*)
				_help
				exit 1
		esac
		shift
	done

	[ ! -r "$propfile" ] && echo "ERROR : $propfile non readeable" && exit 1
	[ -z "$env" -a -z "$pf" ] && echo "ERROR : need to set an environment or a platform" && exit 1
	cat <<-EOF
==============================================
		properties file : $propfile
		environment : $env
		platform : $pf
		version : $version
==============================================
	EOF
}

inject_props_platform() {
	
	echo "Insert properties on platform  $pf"
	
	[ -z "$version" ] && version_sql=NULL
	[ -n "$version" ] && version_sql="'$version'"

	psql -U $DB_USER -d $DB <<-EOF
	BEGIN;
	$(while IFS='=' read -r i j; do
		echo "insert into properties (name,value,platform,version) values('$(trim "$i")',E'$(trim "${j//\\/\\\\}")','$pf',$version_sql);"
	done < <(egrep -v '^( *#|@)' $propfile | egrep '.') ) 
	END;
	EOF
}

inject_props_env() {
	echo "Insert properties on environnment $env"
	[ -z "$version" ] && version_sql=NULL
	[ -n "$version" ] && version_sql="'$version'"
	
	psql -U $DB_USER -d $DB <<-EOF
	BEGIN;
	$(while IFS='=' read -r i j; do
		echo "insert into properties (name,value,version,environment) values ('$(trim "$i")',E'$(trim "${j//\\/\\\\}")',$version_sql,'$env');"
	done < <(egrep -v '^( *#|@)' $propfile | egrep '.') ) 
	END;
	EOF
}

[ $# -lt 1 ] && _help && exit
parse_opts $@
if [ -n "$pf" ]
then
	inject_props_platform
else
	inject_props_env
fi
exit
