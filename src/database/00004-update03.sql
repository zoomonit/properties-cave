
create or replace function listReusedProperties() returns setof propertyinfo
  AS $$
    select * from propertyinfo where name in (select distinct unnest(regexp_matches(value, '@@([^@]+)@@', 'g')) from propertyvalue where value ~ '.*@@.+@@.*');
  $$ LANGUAGE sql;
