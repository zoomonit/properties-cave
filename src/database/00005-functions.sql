create or replace function listPFMissingTheProp(propname text, askedversion text, country text) returns setof text
as $$
  DECLARE
    sqlquery text;
    allpfselect text;
    pfWithPropFoundSQL text;
  BEGIN
        
    allpfselect := ' (select pf.name from platform pf
                     join environment e on e.name = pf.environment
                     where e.role = ''IRIS''';
    IF country IS NOT NULL 
    THEN 
        allpfselect := allpfselect || ' and e.country = $1 '; 
    END IF;
    allpfselect := allpfselect || ') ';
     
    pfWithPropFoundSQL := '(
    -- All platforms if the property is defined for all
      select pf.name from platform pf 
      where exists (select 1 from properties p 
                    where p.platform is null and p.environment is null and p.country is null
                    and p.name = $3
                    and version_between($2,p.version,p.version_limit))
    -- All platforms for a country if the property is defined for a country
    UNION (select pf.name from platform pf
           join environment e on e.name = pf.environment
           join properties p on p.country = e.country
           where p.name = $3 ';
    IF country IS NOT NULL 
    THEN 
        pfWithPropFoundSQL := pfWithPropFoundSQL || ' and e.country = $1 '; 
    END IF;
    pfWithPropFoundSQL := pfWithPropFoundSQL || ' and version_between($2,p.version,p.version_limit))
    -- All platforms for an environment if the property is define for the environment
    UNION (select pf.name from platform pf 
            join environment e on e.name = pf.environment
            join properties p on p.environment = pf.environment 
            where p.name = $3 ';
    IF country IS NOT NULL 
    THEN 
        pfWithPropFoundSQL := pfWithPropFoundSQL || ' and e.country = $1 '; 
    END IF;
    pfWithPropFoundSQL := pfWithPropFoundSQL || ' and version_between($2,p.version,p.version_limit))
    -- The specific platform
    UNION (select platform 
            from properties p
            where platform is not null 
            and name = $3
            and version_between($2,p.version,p.version_limit))
     )';
    sqlquery := 'select * from  (' || allpfselect || ' EXCEPT ' || pfWithPropFoundSQL || ') a where exists ' || pfWithPropFoundSQL;
    RETURN QUERY EXECUTE sqlquery USING country, askedversion, propname;
  END;
$$ language 'plpgsql' STABLE;

create or replace function listMissingProperties(askedversion text) returns setof missingproperties
as $$ 
    BEGIN
        RETURN QUERY select prop.name, array(select * from listPFMissingTheProp(prop.name, askedversion, null)) as pflist 
                from (select distinct name from properties) prop 
                where exists(select 1 from listPFMissingTheProp(prop.name, askedversion, null)) group by prop.name;
    END;
$$ language plpgsql STABLE;

create or replace function listMissingProperties(askedversion text, askedcountry text) returns setof missingproperties
as $$ 
    BEGIN
        RETURN QUERY select prop.name, array(select * from listPFMissingTheProp(prop.name, askedversion, askedcountry)) as pflist 
                from  (select distinct name from properties) prop
                where exists(select 1 from listPFMissingTheProp(prop.name, askedversion, askedcountry)) group by prop.name;
    END;
$$ language plpgsql STABLE;

create or replace function listPlatformProperties(plf text) returns setof properties
as $$ 
    DECLARE
    plfexists boolean;
    BEGIN
        -- check if platform exists
        SELECT exists(select name from platform where name ilike plf) INTO plfexists ;
        IF NOT plfexists THEN
            RAISE EXCEPTION 'Platform % does not exist !', plf;
        ELSE
            RETURN QUERY select name,value,id,platform,version,version_limit, description,environment,country,editor,lastupdate,valuetype,note,template,domain
            from (
                    -- world_level
                    (select * from properties where platform is null and environment is null and country is null) 
                    -- country_level
                    UNION (select p.* from properties p 
                           join country c on p.country = c.name
                           join environment e on e.country = c.name
                           join platform pf on pf.environment = e.name and pf.name ilike plf)
                    -- environment_level
                    UNION (select p.*
                        from properties p 
                        join platform pf on pf.environment = p.environment and pf.name ilike plf)
                    -- platform_level
                    UNION (select p.*
                        from properties p 
                        where p.platform ilike plf) ) 
            a;
        END IF;
    END;
$$ language plpgsql STABLE;

create or replace function listPlatformProperties(plf text, forversion text) returns setof properties
as $$ 
    BEGIN   
        IF forversion !~ E'^[\\d\.]+(-SNAPSHOT)?$' THEN
            RAISE EXCEPTION E'version % doesn''t match ''^[\\d\.]+(-SNAPSHOT)?$'' ',forversion;
        ELSE
            RETURN QUERY select name,value,id,platform,version,version_limit, description,environment,country,editor,lastupdate,valuetype,note,template,domain
            from (select *, rank() over( partition by name order by platform,environment,country) as rank 
              from listPlatformProperties(plf) as list 
              where version_between(forversion,list.version,list.version_limit)
            ) a where rank = 1;
        END IF;
    END;
$$ language plpgsql STABLE;

create or replace function generatePropertiesFile(plf text, forversion text) returns setof text
as $$ 
    select * from ((select '@include ' || filename from includes where platform = $1) 
                   UNION (select name || '=' || value from listplatformproperties($1,$2) where template is null)) a(line)  
    order by line COLLATE "C";
$$ language sql STABLE;
