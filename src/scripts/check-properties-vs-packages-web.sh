#!/bin/bash

. $(dirname "$0")/lib/common.sh

PATH=$PATH:/var/www/cgi-bin
export PATH

# Envoi du type MIME au browser (obligatoire)
#
echo "Content-Type: text/event-stream"
echo "Cache-Control: no-cache"

[ "$REQUEST_METHOD" == "GET" ] && params=$QUERY_STRING
[ "$REQUEST_METHOD" == "POST" ] && read params

setQueryVars $params

if [ -z "$CGI_platform" ]
then
	echo "Status: 405 ERREUR: pas de 'platform' entrée !"
	echo ""
	exit 1
fi

if [ -z "$CGI_version" ]
then
	echo "Status: 405 ERREUR: pas de 'version' entrée !"
	echo ""
	exit 1
fi


echo ""
checkpropsFile.sh "/opt/ipc/workspace/$CGI_version" "$CGI_platform" "$CGI_version" 2>&1 | while read line 
do
    echo "id: $(date +%s%N)"
    echo "data: $(echo "$line" | sed -e 's/^/{ "header" : "/' -r -e 's/\o33\[4[123]m +([A-Z]+) +\o33\[m/", "level" : "\1","log" : "/' -e 's/$/"}/')"
    echo ""
done


