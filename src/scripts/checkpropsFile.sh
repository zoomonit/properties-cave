#!/bin/bash

. /usr/local/bin/lib/common.sh 
. ./shellotemplatator

WORKDIR=$1
PF=$2
VERSION=$3
FILENAME="${WORKDIR}/$PF-$VERSION-$(date +%y%m%d).iris.fortesting"

[ $# -ne 3 ] && log_error "USAGE : $0 <workdir> <pf> <version>" && exit 1


mkdir -p "$WORKDIR"

user="dev"
password="CNbitsUHard"
serverurl="https://gdp.strator.eu"
curl --silent -u "$user:$password" -k -XGET \
    "$serverurl/dwarfgarden/ipc.php?action=getPlatformPropertiesFile&platform=$PF&version=$VERSION&useInclude=false" > "$FILENAME" ;r=$?

[ $r -ne 0 ] && log_error "error generating the prop file " && exit 1

./download-release-packages.sh $PF $VERSION "$WORKDIR" >/dev/null ; r=$?
[ $r -ne 0 ] && log_error "error downloading the packages " && exit 1

(for f in $(find "$WORKDIR" -maxdepth 1 -name '*.zip' -type f)
do 
    log_info "Working on $f"
    mkdir -p "$WORKDIR/tmp"
    unzip -q $f -d "$WORKDIR/tmp"
    [ -d "$WORKDIR/tmp/template" ] && detemplatize $FILENAME "$WORKDIR/tmp/template"
    [ -w "$WORKDIR/tmp/vente-$VERSION/VentePSInstaller/Vente/Strator.Iris.MainApp.exe.config" ] && detemplatize $FILENAME "$WORKDIR/tmp/vente-$VERSION/VentePSInstaller/Vente/Strator.Iris.MainApp.exe.config"
    [ -w "$WORKDIR/tmp/vente-$VERSION/Strator.Installation/Strator.Installation.Main.exe.config" ] && detemplatize $FILENAME "$WORKDIR/tmp/vente-$VERSION/Strator.Installation/Strator.Installation.Main.exe.config"
    [ -w "$WORKDIR/tmp/vente-$VERSION/PSFirstInstallPackage/FirstInstall-IrisVente.config" ] && detemplatize $FILENAME "$WORKDIR/tmp/vente-$VERSION/PSFirstInstallPackage/FirstInstall-IrisVente.config"
    rm -rf "$WORKDIR/tmp"
done ) | tee "$WORKDIR/missings"
# | sort -u | sed -e 's/@//g'

[ ! -s "$WORKDIR/missings" ] && log_info "Aucune propriété ne manque." && exit 0

log_warn "Les propriétés suivantes sont manquantes. "
log_warn "=========================================="
cat $WORKDIR/missings | sort -u | sed -e 's/@@//g'


