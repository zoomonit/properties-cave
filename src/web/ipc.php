<?php

require_once('log4php/Logger.php');
Logger::configure('log4php.xml');

include('ipclib.php');

$logger = Logger::getLogger("main");

$dgm = new IPCManager();


// Get the request arrays
$parameters = array();

function set_headers($code,$message){
    header("HTTP/1.1 ".$code." ".$message);
    header("Content-Type: text/json");
}

function displayErrorPage($message) {
    header("HTTP/1.1 500 ");
    header("Content-Type: text/json");
    echo "";
    echo $message;
}

// first of all, pull the GET vars
if (isset($_SERVER['QUERY_STRING'])) {
    parse_str($_SERVER['QUERY_STRING'], $parameters);
}

// now how about PUT/POST bodies? These override what we got from GET
$body = file_get_contents("php://input");

if (!isset($parameters['action'])) {
    set_headers("405","'action' parameter not set !");
} else {
    set_headers("200","OK");
    $dgm->setParameters($parameters,json_decode($body));
    try {
        $actionoutput = $dgm->{$parameters['action']}();
        echo $actionoutput;
    } catch (Throwable $t) {
        displayErrorPage($t->getMessage());
        $logger->error("error processing the request", $t);
        throw $t;
    } catch (Exception $e) {
        displayErrorPage($e->getMessage());
        $logger->error("error processing the request", $e);
        throw $e;
    }
    
    $dgm->resetParameters();
}

?>
