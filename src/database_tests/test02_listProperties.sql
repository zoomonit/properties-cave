BEGIN;
--
-- Populate propertyinfo
INSERT INTO propertyinfo (name) VALUES ('prop1');
INSERT INTO propertyinfo (name) VALUES ('prop2');

-- Populate propertyvalue
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop1','V11','DEV1','2.10','2.20',NULL,NULL);
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop1','V12',NULL,NULL,NULL,'DEV',NULL);
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop1','V13',NULL,'2.00','3.00',NULL,NULL);
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop1','V14',NULL,NULL,NULL,NULL,'FR');
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop1','V15',NULL,NULL,NULL,NULL,'ES');
INSERT INTO propertyvalue (name, value, platform, version, version_limit, environment, country) VALUES ('prop2','V20',NULL,NULL,NULL,NULL,'ES');

DO $$
  DECLARE result VARCHAR;
BEGIN
  result := (SELECT count(*)::VARCHAR FROM listplatformproperties('DEV1'));
  PERFORM assert_same( '4', result );

  result := (SELECT value FROM listplatformproperties('DEV1','2.12'));
  PERFORM assert_same( 'V11', result );
  result := (SELECT value FROM listplatformproperties('DEV1','1.00'));
  PERFORM assert_same( 'V12', result );
  result := (SELECT value FROM listplatformproperties('DEV1','2.50'));
  PERFORM assert_same( 'V12', result );
  result := (SELECT value FROM listplatformproperties('DEV1','3.00'));
  PERFORM assert_same( 'V12', result );

END $$;
ROLLBACK;
